<?php
class Donations extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('donation_model');
	}

	public function index()
	{
		if(!$this->session->userdata('logged_in')){
			redirect('users/login');
		}
		$data['donations'] = $this->donation_model->get_donation_requests();
		$data['title'] = "Donation requests";

		$this->load->view('templates/header', $data);
		$this->load->view('donations/index', $data);
		$this->load->view('templates/footer', $data);

	}

	public function create()
	{
		if($this->session->userdata('type') == 'Rescue team'){
			redirect('donations');
		}

		$data['title'] = 'Donate to help our mission!';

		$this->form_validation->set_rules('donation_request', 'Donation time and place', 'required');
		$this->form_validation->set_rules('donation_text', 'Donation type', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('donations/create', $data);
			$this->load->view('templates/footer');

		}
		else
		{
			$this->donation_model->give_donation();
			$this->session->set_flashdata('donation_added', 'Your donation request has been registered. Thank you!');
			redirect('adopting_pets');
		}
	}

}
