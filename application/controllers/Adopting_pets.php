<?php
class Adopting_pets extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('adopting_pet_model');
		$this->load->helper('url_helper');
	}

	public function index()
	{
		$data['adopting_pets'] = $this->adopting_pet_model->get_pets();
		$data['title'] = "Pets for adoption";
		//print_r($this->session->userdata('type'));

		$this->load->view('templates/header', $data);
		$this->load->view('adopting_pets/index', $data);
		$this->load->view('templates/footer', $data);

	}

	public function view($slug = NULL)
	{
		$data['adopting_pet'] = $this->adopting_pet_model->get_pets($slug);
		$animal_id = $data['adopting_pet']['id_animal'];
		$data['adoption_application'] = $this->adoption_application_model->get_applications($animal_id);

		if (empty($data['adopting_pet']))
		{
			show_404();
		}

		$data['title'] = $data['adopting_pet']['name'];


		$this->load->view('templates/header', $data);
		$this->load->view('adopting_pets/view', $data);
		$this->load->view('templates/footer');

	}

	public function create()
	{
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}

		$data['title'] = 'Add a pet up for adoption';

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('animal_type', 'Animal type', 'required');
		$this->form_validation->set_rules('breed_animal', 'Animal breed', 'required');
		$this->form_validation->set_rules('sex_animal', 'Animal gender', 'required');
		$this->form_validation->set_rules('location', 'Location', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('adopting_pets/create', $data);
			$this->load->view('templates/footer');

		}
		else
		{
			$config['upload_path'] = './pet_images';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['max_width'] = '2000';
			$config['max_height'] = '2000';

			$this->load->library('upload', $config);

			if(!($this->upload->do_upload())){
				$errors = array('error' => $this->upload->display_errors());
				$image = 'noimage.jpg';
			} else {
				$data = array('upload_data' => $this->upload->data());
				$image = $_FILES['userfile']['name'];
			}

			$this->adopting_pet_model->add_pet($image);
			$this->session->set_flashdata('pet_added', 'The pet has been added for adoption');
			redirect('adopting_pets');
		}
	}

	public function delete($id_animal)
	{
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}
		$this->adopting_pet_model->delete_pet($id_animal);
		$this->session->set_flashdata('pet_deleted', 'The pet has been removed from adoptions');
		redirect('adopting_pets');
	}

	public function edit($slug)
	{
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}
		$data['adopting_pet'] = $this->adopting_pet_model->get_pets($slug);
		if (empty($data['adopting_pet']))
		{
			show_404();
		}

		$data['title'] = 'Edit pet data';

		$this->load->view('templates/header', $data);
		$this->load->view('adopting_pets/edit', $data);
		$this->load->view('templates/footer');


	}

	public function update()
	{
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}
		$this->adopting_pet_model->update_pet();
		$this->session->set_flashdata('pet_updated', 'Pet data has been updated');
		redirect('adopting_pets');
	}

}
