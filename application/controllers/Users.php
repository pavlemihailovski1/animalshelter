<?php
class Users extends CI_Controller{
	public function register()
	{
		$data['title'] = 'Register';

		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email');
		$this->form_validation->set_rules('postalcode', 'Postal code', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password]');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('users/register', $data);
			$this->load->view('templates/footer');
		} else {
			$enc_password = md5($this->input->post('password'));
			$this->user_model->register($enc_password);

			$this->session->set_flashdata('user_registered', 'Your account has been created');

			redirect('adopting_pets');
		}
	}
	public function login()
	{
		$data['title'] = 'Log in';


		$this->form_validation->set_rules('email', 'Email', 'required');

		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('users/login', $data);
			$this->load->view('templates/footer');
		} else {
			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));

			$user = $this->user_model->login($email, $password);

			if($user){
				$user_data = array(
					'id_user' => $user['id_user'],
					'email' => $email,
					'type' => $user['type'],
					'logged_in' => true
				);

				$this->session->set_userdata($user_data);

				$this->session->set_flashdata('user_logged', 'Login successful');

				redirect('adopting_pets');

			} else {
				$this->session->set_flashdata('login_error', 'Login is incorrect');

				redirect('users/login');
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('type');

		// Set message
		$this->session->set_flashdata('logged_out', 'Log out successful');

		redirect('users/login');
	}

	public function check_email($email)
	{
		$this->form_validation->set_message('check_email', 'Email is taken');
		if($this->user_model->check_email($email)){
			return true;
		} else {
			return false;
		}
	}
}
