<?php
class Tip_reports extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('tip_report_model');
	}

	public function index()
	{
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}
		$data['tip_reports'] = $this->tip_report_model->get_reports();
		$data['title'] = "Tip reports";

		$this->load->view('templates/header', $data);
		$this->load->view('tip_reports/index', $data);
		$this->load->view('templates/footer', $data);

	}

	public function create()
	{
		if($this->session->userdata('type') == 'Rescue team'){
			redirect('tip_reports');
		}
		if(!$this->session->userdata('logged_in')){
			redirect('users/login');
		}

		$data['title'] = 'Send us a tip report about an animal that needs help';

		$this->form_validation->set_rules('text', 'Description text', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('tip_reports/create', $data);
			$this->load->view('templates/footer');

		}
		else
		{
			$config['upload_path'] = './tip_images';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size'] = '2048';
			$config['max_width'] = '2000';
			$config['max_height'] = '2000';

			$this->load->library('upload', $config);

			if(!($this->upload->do_upload())){
				$errors = array('error' => $this->upload->display_errors());
				$image = 'noimage.jpg';
			} else {
				$data = array('upload_data' => $this->upload->data());
				$image = $_FILES['userfile']['name'];
			}

			$this->tip_report_model->add_report($image);
			$this->session->set_flashdata('report_added', 'The report has been added for consideration');
			redirect('adopting_pets');
		}
	}

	public function delete($id_report)
	{
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}
		$this->tip_report_model->delete_report($id_report);
		$this->session->set_flashdata('report_deleted', 'The report has been successfully removed');
		redirect('tip_reports');
	}

}
