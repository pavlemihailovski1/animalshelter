<?php
class Volunteers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('volunteer_model');
	}

	public function index()
	{
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}
		$data['volunteers'] = $this->volunteer_model->get_volunteers();
		$data['title'] = "Volunteer requests";

		$this->load->view('templates/header', $data);
		$this->load->view('volunteers/index', $data);
		$this->load->view('templates/footer', $data);

	}

	public function create()
	{
		if($this->session->userdata('type') == 'Rescue team'){
			redirect('volunteers');
		}
		if($this->session->userdata('type') != 'Rescue team'){
			redirect('users/login');
		}

		$data['title'] = 'Join us by becoming one of our centers volunteer!';

		$this->form_validation->set_rules('location', 'Location', 'required');
		$this->form_validation->set_rules('age', 'Age', 'required');
		$this->form_validation->set_rules('text', 'Description text', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('volunteers/create', $data);
			$this->load->view('templates/footer');

		}
		else
		{
			$this->volunteer_model->add_volunteer();
			$this->session->set_flashdata('volunteer_added', 'Your volunteer request has been added for consideration. Thank you!');
			redirect('adopting_pets');
		}
	}
}
