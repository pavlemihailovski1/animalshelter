<?php
class Adoption_applications extends CI_Controller{
	public function adopt($animal_id)
	{
		if(!$this->session->userdata('logged_in')){
			redirect('users/login');
		}

		$slug = $this->input->post('slug');
		$user_id = $this->session->userdata('user_id');
		$data['adopting_pet'] = $this->adopting_pet_model->get_pets($slug);

		$this->form_validation->set_rules('text', 'Text', 'required');
		$this->form_validation->set_rules('residence', 'Residence', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');


		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('adopting_pets/view', $data);
			$this->load->view('templates/footer');
		} else {
			$this->adoption_application_model->create_application($animal_id, $user_id);
			redirect('adopting_pets/'.$slug);
		}
	}
}
