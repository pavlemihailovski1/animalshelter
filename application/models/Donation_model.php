<?php
class Donation_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_donation_requests($slug = FALSE)
	{
		if ($slug === FALSE) {
			$this->db->order_by('id_donation', 'DESC');
			$query = $this->db->get('Donation');
			return $query->result_array();
		}

		$query = $this->db->get_where('Donation', array('slug' => $slug));
		return $query->row_array();
	}

	public function give_donation()
	{
		$slug = url_title($this->input->post('id_donation'));

		$data = array(
			'slug' => $slug,
			'donation_request' => $this->input->post('donation_request'),
			'donation_text' => $this->input->post('donation_text'),
			'user_id' => $this->session->userdata('id_user')
		);
		return $this->db->insert('Donation', $data);
	}
}
