<?php
class Adoption_application_model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}

	public function create_application($animal_id, $user_id)
	{
		$data = array(
			'animal_id' => $animal_id,
			'text' => $this->input->post('text'),
			'residence' => $this->input->post('residence'),
			'phone' => $this->input->post('phone'),
			'user_id' => $user_id
		);

		return $this->db->insert('Adoption_application', $data);
	}

	public function get_applications($animal_id){

		$query = $this->db->get_where('Adoption_application', array('animal_id' => $animal_id));
		return $query->result_array();

	}
}
