<?php
class User_model extends CI_Model{
	public function register($enc_password)
	{
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'town' => $this->input->post('town'),
			'postalcode' => $this->input->post('postalcode'),
			'email' => $this->input->post('email'),
			'password' => $enc_password
		);

		return $this->db->insert('User', $data);
	}

	public function login($email, $password)
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password);

		$result = $this->db->get('User');

		if($result->num_rows() == 1){
			$tmp = array(
				'id_user' => $result->row(0)->id_user,
				'type' => $result->row(0)->type
			);
			return $tmp;
		} else {
			return false;
		}
	}

	public function get_user($id_user){
		$this->db->where('id_user', $id_user);

		$result = $this->db->get('User');
		if($result->num_rows() == 1) {
			$tmp = array(
				'first_name' => $result->row(0)->first_name,
				'last_name' => $result->row(0)->last_name
			);
			return $tmp['first_name'].' '.$tmp['last_name'];
		} else return '';
	}
	public function get_user_contact($id_user){
		$this->db->where('id_user', $id_user);

		$result = $this->db->get('User');
		if($result->num_rows() == 1) {
			return $result->row(0)->email;
		} else return '';
	}


	public function check_email($email)
	{
		$query = $this->db->get_where('User', array('email' => $email));
		if(empty($query->row_array())){
			return true;
		} else {
			return false;
		}
	}
}
