<?php
class Tip_report_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_reports($slug = FALSE)
	{
		if ($slug === FALSE) {
			$this->db->order_by('id_report', 'DESC');
			$query = $this->db->get('Tip_report');
			return $query->result_array();
		}

		$query = $this->db->get_where('Tip_report', array('slug' => $slug));
		return $query->row_array();
	}

	public function add_report($image)
	{
		$slug = url_title($this->input->post('id_report'));

		$data = array(
			'slug' => $slug,
			'text' => $this->input->post('text'),
			'user_id' => $this->session->userdata('id_user'),
			'image' => $image
		);
		return $this->db->insert('Tip_report', $data);
	}
	public function delete_report($id_report)
	{
		$this->db->where('id_report', $id_report);
		$this->db->delete('Tip_report');
		return true;
	}
}
