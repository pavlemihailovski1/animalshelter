<?php
class Adopting_pet_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_pets($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$this->db->order_by('id_animal', 'DESC');
			$query = $this->db->get('Adopting_pet');
			return $query->result_array();
		}

		$query = $this->db->get_where('Adopting_pet', array('slug' => $slug));
		return $query->row_array();
	}

	public function add_pet($image)
	{
		$slug = url_title($this->input->post('name'));

		$data = array(
			'name' => $this->input->post('name'),
			'slug' => $slug,
			'born_date' => date( 'Y-m-d', strtotime( $this->input->post('born_date') ) ),
			'rescue_date' => date( 'Y-m-d', strtotime( $this->input->post('rescue_date') ) ),
			'animal_type' => $this->input->post('animal_type'),
			'breed_animal' => $this->input->post('breed_animal'),
			'sex_animal' => $this->input->post('sex_animal'),
			'color' => $this->input->post('color'),
			'location' => $this->input->post('location'),
			'history_animal' => $this->input->post('history_animal'),
			'user_id' => $this->session->userdata('id_user'),
			'image' => $image
		);
		return $this->db->insert('Adopting_pet', $data);
	}

	public function delete_pet($id_animal)
	{
		$this->db->where('id_animal', $id_animal);
		$this->db->delete('Adopting_pet');
		return true;
	}

	public function update_pet()
	{
		$slug = url_title($this->input->post('name'));
		$data = array(
			'name' => $this->input->post('name'),
			'slug' => $slug,
			'color' => $this->input->post('color'),
			'location' => $this->input->post('location'),
			'history_animal' => $this->input->post('history_animal')
		);
		$this->db->where('id_animal', $this->input->post('id_animal'));
		return $this->db->update('Adopting_pet', $data);
	}
}
