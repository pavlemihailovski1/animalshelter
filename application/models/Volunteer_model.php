<?php
class Volunteer_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_volunteers($slug = FALSE)
	{
		if ($slug === FALSE) {
			$this->db->order_by('id_volunteer', 'DESC');
			$query = $this->db->get('Volunteer');
			return $query->result_array();
		}

		$query = $this->db->get_where('Volunteer', array('slug' => $slug));
		return $query->row_array();
	}

	public function add_volunteer()
	{
		$slug = url_title($this->input->post('id_volunteer'));

		$data = array(
			'slug' => $slug,
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'age' => $this->input->post('age'),
			'location' => $this->input->post('location'),
			'text' => $this->input->post('text'),
			'user_id' => $this->session->userdata('id_user')
		);
		return $this->db->insert('Volunteer', $data);
	}
}
