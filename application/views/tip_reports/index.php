<h2><?php echo $title; ?></h2>

<?php foreach ($tip_reports as $tip_report): ?>

	<div class="row card">
		<div class="col-md-4">
			<img class="img-thumbnail" src="<?php echo site_url(); ?>tip_images/<?php echo $tip_report['image']; ?>"  width="500px">
		</div>
		<div class="col-md-8">
			<div class="main">
				<p>Report sent on: <?php echo $tip_report['date']; ?> by <strong><?php echo $this->user_model->get_user($tip_report['user_id']); ?></strong></p><br>
				<?php echo $tip_report['text']; ?>
			</div>
			<?php if($this->session->userdata('logged_in') && $this->session->userdata('type') == 'Rescue team'): ?>
				<!-- delete button -->
				<?php echo form_open('/tip_reports/delete/'.$tip_report['id_report']); ?>
				<input type="submit" value="Delete" class="btn btn-outline-danger">
				</form>
			<?php endif; ?>
		</div>
	</div>

<?php endforeach; ?>
