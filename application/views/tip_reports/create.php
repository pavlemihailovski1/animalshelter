<h3><?= $title; ?></h3>

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('tip_reports/create'); ?>
<div class="form-group">
	<label>Report description</label>
	<textarea class="form-control" rows="3" name="text" placeholder="Report on the animal that needs help"></textarea>
</div>
<div class="form-group">
	<label>Report Image</label>
	<input type="file" name="userfile" size="20">
</div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>
