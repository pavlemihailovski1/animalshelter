<?php echo validation_errors(); ?>
<?php echo form_open('users/register'); ?>
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<h3 class="text-center"><?= $title; ?></h3>
		<div class="form-group">
				<label>First name</label>
				<input type="text" class="form-control" name="first_name" placeholder="First name">
			</div>
		<div class="form-group">
			<label>Last name</label>
			<input type="text" class="form-control" name="last_name" placeholder="Last name">
		</div>
		<div class="form-group">
			<label>Town</label>
			<input type="text" class="form-control" name="town" placeholder="Your town">
		</div>
		<div class="form-group">
			<label>Postal code</label>
			<input type="text" class="form-control" name="postalcode" placeholder="Postal code">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="email" class="form-control" name="email" placeholder="Email">
		</div>
		<div class="form-group">
			<label>Password</label>
			<input type="password" class="form-control" name="password" placeholder="Password">
		</div>
		<div class="form-group">
			<label>Confirm password</label>
			<input type="password" class="form-control" name="password2" placeholder="Confirm password">
		</div>

		<button type="submit" class="btn btn-primary btn-block">Submit</button>
	</div>
</div>
<?php echo form_close(); ?>
