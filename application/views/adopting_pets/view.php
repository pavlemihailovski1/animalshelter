<h2><?php echo $adopting_pet['name']; ?></h2>
<p class="d-md-inline-block">Created by: <?php echo $this->user_model->get_user($adopting_pet['user_id']); ?></p><br>
<div class="row">
	<div class="col-md-7">
		<img class="img-thumbnail" src="<?php echo site_url(); ?>pet_images/<?php echo $adopting_pet['image']; ?>" width="500"><br>
	</div>

	<div class="col-md-5">
		<p>Type: <?php echo $adopting_pet['animal_type']; ?></p><br>
		<p>Breed: <?php echo $adopting_pet['breed_animal']; ?></p><br>
		<p>Boy/girl: <?php echo $adopting_pet['sex_animal']; ?></p><br>
		<p>Color: <?php echo $adopting_pet['color']; ?></p><br>
		<p>Rescue center: <?php echo $adopting_pet['location']; ?></p><br>

	</div>
</div>
<hr>
<p>Born on: <?php echo $adopting_pet['born_date']; ?> and rescued on: <?php echo $adopting_pet['rescue_date']; ?></p><br>
<h6>Pet's story</h6>
<?php echo $adopting_pet['history_animal']; ?>




<?php if($this->session->userdata('logged_in') && $this->session->userdata('type') == 'Rescue team'): ?>
	<hr>
<div class="row">
	<div class="col-6">
	<a class="btn btn-outline-dark" href="<?php echo base_url(); ?>adopting_pets/edit/<?php echo $adopting_pet['slug']; ?>">Edit data</a>
	</div>
	<div class="col-6">
		<?php echo form_open('/adopting_pets/delete/'.$adopting_pet['id_animal']); ?>
			<input type="submit" value="Delete" class="btn btn-outline-danger">
		</form>
	</div>
</div>
<?php endif; ?>

<?php if($this->session->userdata('logged_in') && $this->session->userdata('type') != 'Rescue team'): ?>
	<hr>
	<h4>Interested in adoption?</h4>
	<p>Fill in the form below</p>
	<?php echo validation_errors(); ?>
	<?php echo form_open('adoption_applications/adopt/'.$adopting_pet['id_animal']); ?>
		<div class="form-group">
			<label>Describe yourself, your living situation and why you wish to adopt?</label>
			<textarea class="form-control" rows="3" name="text" placeholder="Describe"></textarea>
		</div>
		<div class="form-group">
			<label>Your residence</label>
			<input type="text" class="form-control" name="residence" placeholder="Enter your address of residence">
		</div>
		<div class="form-group">
			<label>Contact phone number</label>
			<input type="number" class="form-control" name="phone" placeholder="Insert your phone number">
		</div>
		<input type="hidden" name="slug" value="<?php echo $adopting_pet['slug']; ?>">
		<button class="btn btn-outline-primary" type="submit">Submit application</button>
	</form>
<?php endif; ?>

<?php if($this->session->userdata('logged_in') && $this->session->userdata('type') == 'Rescue team'): ?>
	<hr>
	<h4>Applications</h4>
	<?php if($adoption_application) : ?>
		<?php foreach($adoption_application as $adoption_application) : ?>
			<div class="card">
				<h5>Application submitted by <strong><?php echo $this->user_model->get_user($adoption_application['user_id']); ?></strong></h5>
				<p><?php echo $adoption_application['text']; ?></p>
				<h6>Personal information relating to the adoption request</h6>
				<p>Address: <?php echo $adoption_application['residence']; ?></p>
				<p>Phone number: <?php echo $adoption_application['phone']; ?></p>
			</div>
		<?php endforeach; ?>
	<?php else : ?>
		<p>No applications yet</p>
	<?php endif; ?>
<?php endif; ?>
