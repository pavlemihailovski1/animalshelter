<h3><?= $title; ?></h3>

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('adopting_pets/update'); ?>
<input type="hidden" name="id_animal" value="<?php echo $adopting_pet['id_animal']; ?>">
<div class="form-group">
	<label>Pet's name</label>
	<input type="text" class="form-control" name="name" placeholder="Add name" value="<?php echo $adopting_pet['name']; ?>">
</div>
<div class="form-group">
	<label>Color of fur</label>
	<input type="text" class="form-control" name="color" placeholder="Insert fur color" value="<?php echo $adopting_pet['color']; ?>">
</div>
<div class="form-group">
	<label>Location</label>
	<input type="text" class="form-control" name="location" placeholder="Insert rescue center" value="<?php echo $adopting_pet['location']; ?>">
</div>
<div class="form-group">
	<label>Pet's story</label>
	<textarea class="form-control" rows="3" name="history_animal" placeholder="Describe their story :("><?php echo $adopting_pet['history_animal']; ?></textarea>
</div>

<button type="submit" class="btn btn-primary">Add</button>
</form>
