<h2><?php echo $title; ?></h2>

<?php foreach ($adopting_pets as $adopting_pet): ?>
<hr>
	<h3><?php echo $adopting_pet['name']; ?></h3>

	<div class="row">
		<div class="col-md-4">
			<img class="img-thumbnail" src="<?php echo site_url(); ?>pet_images/<?php echo $adopting_pet['image']; ?>"  width="300px">
		</div>
		<div class="col-md-8">
			<div class="main">
				<p>Type: <?php echo $adopting_pet['animal_type']; ?></p><br>
				<p>Breed: <?php echo $adopting_pet['breed_animal']; ?></p><br>
				<p>Boy/girl: <?php echo $adopting_pet['sex_animal']; ?></p><br>
				<p>Color: <?php echo $adopting_pet['color']; ?></p><br>
				<p>Rescue center: <?php echo $adopting_pet['location']; ?></p><br>
			</div>
			<p><a class="btn btn-outline-dark" href="<?php echo site_url('adopting_pets/'.$adopting_pet['slug']); ?>">View pet</a></p>
		</div>
	</div>

<?php endforeach; ?>
