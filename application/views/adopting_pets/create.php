<h3><?= $title; ?></h3>

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('adopting_pets/create'); ?>
<div class="form-group">
	<label>Pet's name</label>
	<input type="text" class="form-control" name="name" placeholder="Add name">
</div>
<div class="form-group">
	<label>Date of birth</label>
<input type="date" class="form-control" name="born_date" value="<?php echo isset($itemOutData->born_date) ? set_value('born_date', date('Y-m-d', strtotime($itemOutData->born_date))) : set_value('born_date'); ?>">
</div>
<div class="form-group">
	<label>Rescue date</label>
	<input type="date" class="form-control" name="rescue_date" value="<?php echo isset($itemOutData->rescue_date) ? set_value('rescue_date', date('Y-m-d', strtotime($itemOutData->rescue_date))) : set_value('rescue_date'); ?>">
</div>
	<div class="form-group">
		<label>Dog or cat?</label>
	<input type="text" class="form-control" name="animal_type" placeholder="Dog or cat?">
</div>
<div class="form-group">
	<label>Breed</label>
	<input type="text" class="form-control" name="breed_animal" placeholder="Insert breed">
</div>
<div class="form-group">
	<label>Boy or girl?</label>
	<input type="text" class="form-control" name="sex_animal" placeholder="Boy or girl?">
</div>
<div class="form-group">
	<label>Color of fur</label>
	<input type="text" class="form-control" name="color" placeholder="Insert fur color">
</div>
<div class="form-group">
	<label>Location</label>
	<input type="text" class="form-control" name="location" placeholder="Insert rescue center">
</div>
<div class="form-group">
	<label>Pet's story</label>
	<textarea class="form-control" rows="3" name="history_animal" placeholder="Describe their story :("></textarea>
</div>

<div class="form-group">
	<label>Pet Image</label>
	<input type="file" name="userfile" size="20">
</div>

<button type="submit" class="btn btn-primary">Add</button>
</form>
