<h3>Our mission</h3>
<div>
<p>Having an online place where people can make an account and have the opportunity to give useful information,
	to become a volunteer, future pet owner or a donator, will be a step closer to decreasing the number of homeless pets.
	By adopting them we are one step closer to reduce the number of animals on the streets. The system will be able to help us
	find them a home and also to attract future pet owners to send us a request to adopt a pet. </p>
</div>
<hr>

<h5>Contact us!</h5>
<h6>These are our rescue center locations:</h6><br>
<ul>
	<li>
		<p>Prvomajska 23, 2326 Pehcevo</p>
		<p>Contact phone: 078286745</p><br>
	</li>
	<li>
		<p>Cevljarska ulica 15, 6000 Koper</p>
		<p>Contact phone: 040286746</p><br>
	</li>
	<li>
		<p>Potrceva ulica 2, 2000 Maribor</p>
		<p>Contact phone: 070286700</p><br>
	</li>
	<li>
		<p>Zupanciceva ulica 43, 1000 Ljubljana</p>
		<p>Contact phone: 030286740</p><br>
	</li>
</ul>
