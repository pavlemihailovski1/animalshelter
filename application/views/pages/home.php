<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100" src="<?php echo base_url(); ?>assets/firstslide.jpg" alt="First slide">
			<div class="carousel-caption d-none d-md-block">
				<h1>Animal Shelter and Rescue Centers</h1>
				<h6>Welcome!</h6>
			</div>
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="<?php echo base_url(); ?>assets/secondslide.jpg" alt="Second slide">
			<div class="carousel-caption d-none d-md-block">
				<h1>Animal Shelter and Rescue Centers</h1>
				<h6>Welcome!</h6>
			</div>
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="<?php echo base_url(); ?>assets/thirdslide.jpg" alt="Third slide">
			<div class="carousel-caption d-none d-md-block">
				<h1>Animal Shelter and Rescue Centers</h1>
				<h6>Welcome!</h6>
			</div>
		</div>
	</div>
</div>

<hr>

<h5>Contact us!</h5>
<h6>These are our rescue center locations:</h6><br>
<ul>
	<li>
		<p>Prvomajska 23, 2326 Pehcevo</p>
		<p>Contact phone: 078286745</p><br>
	</li>
	<li>
		<p>Cevljarska ulica 15, 6000 Koper</p>
		<p>Contact phone: 040286746</p><br>
	</li>
	<li>
		<p>Potrceva ulica 2, 2000 Maribor</p>
		<p>Contact phone: 070286700</p><br>
	</li>
	<li>
		<p>Zupanciceva ulica 43, 1000 Ljubljana</p>
		<p>Contact phone: 030286740</p><br>
	</li>
</ul>

<hr>
<div class="row card text-center">
<h4>We are accepting donations! </h4>
	<div class="card-body">
		<a class="btn btn-primary" href="<?php echo base_url(); ?>donations/create" role="button">Donate</a>
	</div>
</div>

<hr>
