<h3><?= $title; ?></h3>
<div class="card">
	<h6>Donate funding for our shelters! </h6>
	<p>Contribute to our mission and allows us to help every one of the animals on the streets</p><br>
	<div class="container p-0 col-6">
		<div class="card px-4">
			<p class="h8 py-3">Payment Details</p>
			<div class="row gx-3">
				<div class="col-12">
					<div class="d-flex flex-column">
						<p class="text mb-1">Person Name</p>
						<input class="form-control mb-3" type="text" placeholder="Name" value="Barry Allen">
					</div>
				</div>
				<div class="col-12">
					<div class="d-flex flex-column">
						<p class="text mb-1">Card Number</p>
						<input class="form-control mb-3" type="text" placeholder="1234 5678 435678">
					</div>
				</div>
				<div class="col-6">
					<div class="d-flex flex-column">
						<p class="text mb-1">Expiry</p>
						<input class="form-control mb-3" type="text" placeholder="MM/YYYY">
					</div>
				</div>
				<div class="col-6">
					<div class="d-flex flex-column">
						<p class="text mb-1">CVV/CVC</p>
						<input class="form-control mb-3 pt-2 " type="password" placeholder="***">
					</div>
				</div>
				<div class="col-12">
					<div class="btn btn-primary mb-3">
						<span class="ps-3">Donate $5</span>
						<span class="fas fa-arrow-right"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<hr>
<?php if($this->session->userdata('logged_in') && $this->session->userdata('type') == 'Registered user') : ?>
	<h6>Donate supplies for our shelters! </h6>
<?php echo validation_errors(); ?>

<?php echo form_open_multipart('donations/create'); ?>
<div class="form-group">
	<label>PLease provide the rescue center and the time and date where you will drop off your donation</label>
	<input type="text" class="form-control" name="donation_request" placeholder="Insert rescue center and time">
</div>
<div class="form-group">
	<label>Type of donation</label>
	<input type="text" class="form-control" name="donation_text" placeholder="Example: food, cleaning supplies, transporters, etc.">
</div>

<button type="submit" class="btn btn-primary">Submit</button>
</form>

<?php endif; ?>


