<h2><?php echo $title; ?></h2>

<?php foreach ($donations as $donation): ?>
	<hr>
	<div class="row card">
		<p>Donation request sent by <strong><?php echo $this->user_model->get_user($donation['user_id']); ?></strong> on <?php echo $donation['date']; ?></p>
		<div class="col-md-8">
			<div class="main">
				<p><strong>Type of donation: </strong><?php echo $donation['donation_text']; ?></p>
				<p><strong>Donation request: </strong><?php echo $donation['donation_request']; ?></p>
			</div>
		</div>
	</div>

<?php endforeach; ?>
