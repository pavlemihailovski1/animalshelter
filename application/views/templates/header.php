<html>
<head>
	<title>Animal Shelter</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/minty/bootstrap.min.css" integrity="sha384-H4X+4tKc7b8s4GoMrylmy2ssQYpDHoqzPa9aKXbDwPoPUA3Ra8PA5dGzijN+ePnH" crossorigin="anonymous">
	<link rel="stylesheet" type = "text/css" href="<?php echo base_url("assets/bootstrap.css"); ?>" />
	<script type="text/javascript" src="<?php echo base_url("assets/bootstrap.js"); ?>"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>
<body>

<!--<h1><?php /*echo $title; */?></h1>-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/logo.png" ></a>
	<a class="navbar-brand" href="<?php echo base_url(); ?>">ASRC</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="<?php echo base_url(); ?>">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url(); ?>about">Contact</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url(); ?>adopting_pets">Pets for adoption</a>
			</li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php if(!$this->session->userdata('logged_in')) : ?>
				<a class="nav-link" href="<?php echo base_url(); ?>users/register">Register</a>
				<a class="nav-link" href="<?php echo base_url(); ?>users/login">Log in</a>
			<?php endif; ?>
			<?php if($this->session->userdata('logged_in') && $this->session->userdata('type') == 'Rescue team') : ?>
				<a class="nav-link" href="<?php echo base_url(); ?>adopting_pets/create">Add pet</a>
				<a class="nav-link" href="<?php echo base_url(); ?>tip_reports">Tip reports</a>
				<a class="nav-link" href="<?php echo base_url(); ?>donations">Donations</a>
				<a class="nav-link" href="<?php echo base_url(); ?>volunteers">Volunteer requests</a>
				<a class="nav-link" href="<?php echo base_url(); ?>users/logout">Log out</a>
			<?php endif; ?>
			<?php if($this->session->userdata('logged_in') && $this->session->userdata('type') == 'Registered user') : ?>
				<a class="nav-link" href="<?php echo base_url(); ?>tip_reports/create">Send a tip</a>
				<a class="nav-link" href="<?php echo base_url(); ?>volunteers/create">Become a volunteer</a>
				<a class="nav-link" href="<?php echo base_url(); ?>users/logout">Log out</a>
			<?php endif; ?>
		</ul>
	</div>
</nav>

<div class="container">
	<?php if($this->session->flashdata('user_registered')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('pet_added')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('pet_added').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('pet_updated')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('pet_updated').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('pet_deleted')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('pet_deleted').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('login_error')): ?>
		<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_error').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('user_logged')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_logged').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('logged_out')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('logged_out').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('report_added')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('report_added').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('report_deleted')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('report_deleted').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('volunteer_added')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('volunteer_added').'</p>'; ?>
	<?php endif; ?>
	<?php if($this->session->flashdata('donation_added')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('donation_added').'</p>'; ?>
	<?php endif; ?>
