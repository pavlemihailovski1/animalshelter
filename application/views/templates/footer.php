</div>
<br>
</body>
<!-- Footer -->
<footer class=" text-center text-white" style="background-color:#6dd8ee;">
	<!-- Grid container -->
	<div class="container p-4">



		<!-- Section: Text -->
		<section class="mb-4">
			<p><strong>ASRC - Animal Shelter and Rescue Centers</strong></p>
		</section>
		<!-- Section: Text -->


		<!-- Section: Links -->
		<section class="">
			<!--Grid row-->
			<div class="row">

				<!--Grid column-->
				<div class="col-6 mb-4 mb-md-0 text-center">
					<h5 class="text-uppercase">Pets & adoption</h5>

					<ul class="list-unstyled mb-0">
						<li>
							<a href="<?php echo base_url(); ?>adopting_pets" class="text-white">Pets</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>users/login" class="text-white">Log in</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>users/register" class="text-white">Create an account</a>
						</li>
					</ul>
				</div>
				<!--Grid column-->

				<!--Grid column-->
				<div class="col-6 mb-4 mb-md-0 text-center">
					<h5 class="text-uppercase">Links</h5>

					<ul class="list-unstyled mb-0">
						<li>
							<a href="<?php echo base_url(); ?>" class="text-white">Home</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>about" class="text-white">Contact us</a>
						</li>
					</ul>
				</div>
				<!--Grid column-->
			</div>
			<!--Grid row-->
		</section>
		<!-- Section: Links -->

	</div>
	<!-- Grid container -->

	<!-- Copyright -->
	<div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
		© 2023 Copyright:
		<a class="text-white" href="<?php echo base_url(); ?>">asrc.com</a>
	</div>
	<!-- Copyright -->

</footer>
<!-- Footer -->
</html>
