<h3><?= $title; ?></h3>

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('volunteers/create'); ?>
<div class="form-group">
	<label>Volunteer first name</label>
	<input type="text" class="form-control" name="first_name" placeholder="Add first name">
</div>
<div class="form-group">
	<label>Volunteer last name</label>
	<input type="text" class="form-control" name="last_name" placeholder="Add last name">
</div>
<div class="form-group">
	<label>Volunteer age</label>
	<input type="number" class="form-control" name="age" placeholder="Add age">
</div>
<div class="form-group">
	<label>Location of desired volunteering - rescue center where the volunteer wishes to participate in</label>
	<input type="text" class="form-control" name="location" placeholder="Insert rescue center">
</div>
<div class="form-group">
	<label>Please describe yourself, your time and skills available</label>
	<textarea class="form-control" rows="3" name="text" placeholder="Describe"></textarea>
</div>

<button type="submit" class="btn btn-primary">Submit</button>
</form>
