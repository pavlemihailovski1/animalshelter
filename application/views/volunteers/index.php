<h2><?php echo $title; ?></h2>

<?php foreach ($volunteers as $volunteer): ?>
<hr>
	<div class="row card">
		<p>Report sent by <strong><?php echo $this->user_model->get_user($volunteer['user_id']); ?></strong> (<?php echo $this->user_model->get_user_contact($volunteer['user_id']); ?>)</p>
		<div class="col-md-8">
			<div class="main">
				<p>First name: <strong><?php echo $volunteer['first_name']; ?></strong></p><br>
				<p>Last name: <strong><?php echo $volunteer['last_name']; ?></strong></p><br>
				<p>Age: <strong><?php echo $volunteer['age']; ?></strong></p><br>
				<p>Location of desired volunteering: <strong><?php echo $volunteer['location']; ?></strong></p><br>
				<p><strong>Description: </strong><?php echo $volunteer['text']; ?></p>
			</div>
		</div>
	</div>

<?php endforeach; ?>
